import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import {AngularFireModule} from 'angularfire2';
import {AngularFireAuthModule, AngularFireAuth} from 'angularfire2/auth';
import {AngularFireDatabaseModule, AngularFireDatabase} from 'angularfire2/database';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PostServiceProvider } from '../providers/post-service/post-service';
import { LoginPage } from '../pages/login/login';
import { HTTP } from '@ionic-native/http';
import { Http } from '@angular/http';
import {HttpModule} from '@angular/http'
import { AngularFirestore } from 'angularfire2/firestore';

export const configFirebase = {
  apiKey: "AIzaSyCALfShyKpFT2gEZSnQTAkvAJ0Rml2bNsU",
  authDomain: "moredata-32de8.firebaseapp.com",
  databaseURL: "https://moredata-32de8.firebaseio.com",
  projectId: "socialdata-ac98a",
  storageBucket: "moredata-32de8.appspot.com",
  messagingSenderId: "104317559516"
};

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    AngularFireModule.initializeApp(configFirebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    HttpModule
    

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    PostServiceProvider,
    //AngularFirestore
    //Http
  ]
})
export class AppModule {}
