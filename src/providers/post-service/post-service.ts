import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { AngularFireList, AngularFireDatabase } from "angularfire2/database";
import { AngularFireAuth } from "angularfire2/auth";
import { Post } from "../../models/post";
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Component } from '@angular/core';

// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Category } from '../../models/category';
/*
  Generated class for the PostServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PostServiceProvider {

  posts: Post[] =[];
  results: Post[] = [];
  categories: Category[] = [];

  private commentsUrl = 'http://localhost:10000/analize';

  constructor(public http: Http,
    public postsdb: AngularFireDatabase,
    public afAuth: AngularFireAuth ) {
      console.log('LLega al Servicio');
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');         
        let options = new RequestOptions({ headers: headers }); // Create a request option
        var url = "http://localhost:10000/analize";
        this.http.get(url).map(res => res.json()).subscribe(data => {
          var b:any;
          for(var i in data){                
            console.log(data[i]['Comment_text']);
            this.posts.push(new Post(data[i]["Category"],data[i]["Comment_comments"],data[i]["Comment_date"],data[i]["Comment_id"],data[i]["Comment_likes"],data[i]["Comment_text"],data[i]["Parent_comment_id"],data[i]["Post id"],data[i]["Post_content"],data[i]["key"], data[i]["Has_Phone_Number"], data[i]["Has_Direction"], data[i]["Has_Profile"], data[i]["Sentiment_Value"]));                                           
            for(var c in data[i]){
              console.log(c);              
            }                
            console.log(JSON.stringify(i));  
          }
          console.log(data);  
        });     
    console.log('Carga los datos');
  }     
    
  getPosts(): Post[]{    
    return this.posts;
  }  

  categorize(key: String, category: String){    
    var url = "http://localhost:10000/categorize/"+key+"/"+category;
    this.http.get(url).subscribe(data => { 
      console.log("done!");
    });     
  }

  search(data: String): Post[]{    
    var url = "http://localhost:10000/search/"+data;
    this.http.get(url).map(res => res.json()).subscribe(data => {      
      for(var i in data){                        
        this.results.push(new Post(data[i]["Category"],data[i]["Comment_comments"],data[i]["Comment_date"],data[i]["Comment_id"],data[i]["Comment_likes"],data[i]["Comment_text"],data[i]["Parent_comment_id"],data[i]["Post id"],data[i]["Post_content"],data[i]["key"], data[i]["Has_Phone_Number"], data[i]["Has_Direction"], data[i]["Has_Profile"], data[i]["Sentiment_Value"]));                                                           
      }      
    });  
    return this.results;    
  }

  getCategories(): Category[]{
    var url = "http://localhost:10000/categories";
    this.http.get(url).map(res => res.json()).subscribe(data => {      
      for(var i in data){                        
        this.categories.push(new Category(data[i]["name"],data[i]["key"]));                                                           
      }      
    });  
    return this.categories;   
  }
}
