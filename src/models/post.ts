export class Post {
    
    public comment_id: String;
    public post_id: String;
    public post_content: String;      
    public parent_comment_id: String;
    public comment_date: String;
    public comment_text: String;
    public comment_likes: String;
    public comment_comments: String;
    public key: String;
    public category: String;
    public has_phone_number: String;
    public has_direction: String;
    public has_profile: String;
    public sentiment_value: String;

    // constructor(){

    // }

    constructor(category: String, comment_comments: String, comment_date: String, comment_id: String, comment_likes: String, comment_text: String, parent_comment_id: String, post_id: String, post_content: String, $key: String, has_phone_number: String, has_direction: String, has_profile: String, sentiment_value: String) {
        this.comment_id = comment_id;
        this.post_id = post_id;
        this.post_content = post_content;
        this.parent_comment_id = parent_comment_id;
        this.comment_date = comment_date;
        this.comment_text = comment_text;
        this.comment_likes = comment_likes;
        this.comment_comments = comment_comments;
        this.key = $key;
        this.category = category;
        this.has_direction = has_direction;
        this.has_phone_number = has_phone_number;
        this.has_profile = has_profile;
        this.sentiment_value = sentiment_value;
    }
  }