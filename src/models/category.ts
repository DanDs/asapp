export class Category {
    
    public name: String;
    public key: String;

    constructor( name: String, $key: String) {    
        this.name = name;
        this.key = $key;
    }
  }