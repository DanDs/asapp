import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ViewResultPage } from './view-result';

@NgModule({
  declarations: [
    ViewResultPage,
  ],
  imports: [
    IonicPageModule.forChild(ViewResultPage),
  ],
})
export class ViewResultPageModule {}
