import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, ActionSheetController } from 'ionic-angular';
import { Post } from '../../models/post';
import { AlertController } from 'ionic-angular';
import { PostServiceProvider } from '../../providers/post-service/post-service';

/**
 * Generated class for the ViewResultPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-view-result',
  templateUrl: 'view-result.html',
})
export class ViewResultPage {
  @Input() results: Post[] = [];
  result: Post[] = [];
  @Input() data: String;
  constructor(public navCtrl: NavController, public navParams: NavParams, private alertCtrl: AlertController, public postService: PostServiceProvider, 
              public platform: Platform, public actionsheetCtrl: ActionSheetController) {
    this.results = navParams.get('result');
    this.data = navParams.get('data');
    //this.results = this.postService.search(this.data); 

    this.results = this.SortList(this.results.sort((a,b) => parseFloat(a.sentiment_value.toString()) - parseFloat(b.sentiment_value.toString())));
    
    this.results.reverse();
    //this.results = this.result;
    console.log(this.results);
  }

  SortList(list: Array<Post>){
    
    for (var i=1; i<list.length; i++){
      for(var j=0 ; j<list.length - 1; j++){
        if (parseInt(list[j].sentiment_value.toString()) < parseInt(list[j+1].sentiment_value.toString())){
            var temp = list[j];
            list[j] = list[j+1];
            list[j+1] = temp;
            console.log(parseInt(list[j].sentiment_value.toString()));
        }
      }
    }
    console.log(list);
    return list;
  }

  ionViewDidLoad() {
    console.log(this.results);
    console.log('ionViewDidLoad ViewResultPage');
  }

  search(data: String){
    var result;
    result = this.postService.search(data);
    this.navCtrl.pop();
    this.navCtrl.push('ViewResultPage', {'result': result, 'data': data}); 
    //this.navCtrl.push(ViewResultPage);
  }

  showFilter2(){    
      let alert = this.alertCtrl.create();
      alert.setTitle('Which planets have you visited?');
  
      alert.addInput({
        type: 'checkbox',
        label: 'Alderaan',
        value: 'value1',
        checked: true
      });
  
      alert.addInput({
        type: 'checkbox',
        label: 'Bespin',
        value: 'value2'
      });
  
      alert.addButton('Cancel');
      alert.addButton({
        text: 'Okay',
        handler: data => {
          console.log('Checkbox data:', data);
          //this.testCheckboxOpen = false;
         // this.testCheckboxResult = data;
        }
      });
      alert.present();
    
  }

  showFilter() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Albums',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Delete',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'trash' : null,
          handler: () => {
            console.log('Delete clicked');
          }
        },
        {
          text: 'Share',
          icon: !this.platform.is('ios') ? 'share' : null,
          handler: () => {
            console.log('Share clicked');
          }
        },
        {
          text: 'Play',
          icon: !this.platform.is('ios') ? 'arrow-dropright-circle' : null,
          handler: () => {
            console.log('Play clicked');
          }
        },
        {
          text: 'Favorite',
          icon: !this.platform.is('ios') ? 'heart-outline' : null,
          handler: () => {
            console.log('Favorite clicked');
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  ViewInfo(post: Post){    
      let alert = this.alertCtrl.create({
        title: post.post_content.toString(),
        subTitle: post.comment_text.toString(),
        message: "Likes: ("+post.comment_likes.toString()+")",// + post.sentiment_value.toString(),
        buttons: ['OK']
      });
      alert.present();        
  }  
}
