import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Post } from '../../models/post';
import { PostServiceProvider } from '../../providers/post-service/post-service';
import { HomePage } from '../home/home';
import { Category } from '../../models/category';

/**
 * Generated class for the ClassifyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-classify',
  templateUrl: 'classify.html',
})
export class ClassifyPage {
  @Input() post: Post;
  categories: Category[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, public postService: PostServiceProvider) {
    this.post = navParams.get('post');
    this.categories = postService.getCategories();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClassifyPage');
  }

  classifyPost(classification: String){
      this.postService.categorize(this.post.key,classification);
      //this.navCtrl.push(HomePage);
  }

}
