import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Post } from '../../models/post';
import { PostServiceProvider } from '../../providers/post-service/post-service';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  posts: Post[] = [];  
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public postsService: PostServiceProvider) {        
    //this.messages = messageService.getMessages();
    console.log('LLega al Home'); 
    //this.posts.push(new Post("category","comments","date","id","likes","text","parent_id","post_id","post_content","key"));
    //this.posts = postsService.getPosts();
      this.posts = postsService.getPosts();
      //console.log(this.posts);        
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');    
  }

  Classify(post: Post){
    this.navCtrl.push('ClassifyPage', {'post': post});    
  }
  
}
