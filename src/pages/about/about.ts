import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { PostServiceProvider } from '../../providers/post-service/post-service';
import { ViewResultPage } from '../view-result/view-result';
import { Post } from '../../models/post';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  constructor(public navCtrl: NavController, public postService: PostServiceProvider) {

  }

  search(data: String){
    var result;
    result = this.postService.search(data);
    result = this.SortList(result.sort((a,b) => parseFloat(a.sentiment_value.toString()) - parseFloat(b.sentiment_value.toString())));
    result.reverse();
    this.navCtrl.push('ViewResultPage', {'result': result, 'data': data}); 
    //this.navCtrl.push(ViewResultPage);
  }

  SortList(list: Array<Post>){
    
    for (var i=1; i<list.length; i++){
      for(var j=0 ; j<list.length - 1; j++){
        if (parseInt(list[j].sentiment_value.toString()) < parseInt(list[j+1].sentiment_value.toString())){
            var temp = list[j];
            list[j] = list[j+1];
            list[j+1] = temp;
            console.log(parseInt(list[j].sentiment_value.toString()));
        }
      }
    }
    console.log(list);
    return list;
  }

}
